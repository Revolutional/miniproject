﻿using UnityEngine;
using UnityEngine.SceneManagement;
using System.Collections;

public class changeToGameMenu : MonoBehaviour
{
    public void mainMenuScene (string sceneToChangeTo)
    {
        SceneManager.LoadScene(sceneToChangeTo);
    }
}
