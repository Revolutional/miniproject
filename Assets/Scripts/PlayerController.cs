﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using System.Collections;

public class PlayerController : MonoBehaviour
{
    public float speed;
    public float timeLeft = 10;
    public Text countText;
    public Text timerText;
    public GameObject TriggerDoor;

    private Rigidbody rb;
    private static int scoreCount = 0;

    void Start()
    {
        rb = GetComponent<Rigidbody>();
        PlayerPrefs.GetInt("savedScore", scoreCount);
        //scoreCount = 0; //This caused the score to be set to 0 when a new scene is loaded.
        SetCountText();
        PlayerPrefs.Save();
    }
    void Update()
    {
        Countdown();
        //transform.Translate(Input.acceleration.x, 0, -Input.acceleration.y);
        Vector3 movement = Vector3.zero;
        movement = new Vector3(Input.acceleration.x, 0, Input.acceleration.y);
        rb.AddForce(movement * speed);
        PlayerPrefs.GetInt("saveScore");
    } 
   
    void FixedUpdate()
    {
        
        float moveHorizontal = 0;
        float moveVertical = 0;
        Vector3 movement = Vector3.zero;

       
        
        moveHorizontal = Input.GetAxis("Horizontal");
        moveVertical = Input.GetAxis("Vertical");

        movement = new Vector3(moveHorizontal, 0.0f, moveVertical);



        //moveHorizontal = Input.gyro.userAcceleration.x;
        //moveVertical = Input.gyro.userAcceleration.z;

        //movement = new Vector3(moveHorizontal, 0, moveVertical);

        rb.AddForce(movement * speed);
        
    }

    void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("PickUp"))
        {
            countText.text = "Score: " + scoreCount;
            scoreCount += 1;
            other.gameObject.SetActive(false);
            SetCountText();
        }

        if (other.gameObject.CompareTag("Trigger1"))
        {

            SceneManager.LoadScene("GameScene2");
            if (SceneManager.GetActiveScene().name == "GameScene2")
            {
                PlayerPrefs.Save();
                PlayerPrefs.GetInt("savedScore");
            }
            print(scoreCount);
        }
    }

    void SetCountText()
    {
        countText.text = "Score: " + scoreCount.ToString();
        if (scoreCount >= 5)
        {
            Destroy(TriggerDoor);
        }

        if (scoreCount == 15)
        {
            SceneManager.LoadScene("WinScene");
        }
    }

    void Countdown()
    {
        timeLeft -= Time.deltaTime;
        timerText.text = "Time Left: " + Mathf.Round(timeLeft).ToString();
        if (timeLeft < 0)
        {
            SceneManager.LoadScene("LoseScene");
        }
    }
    
}